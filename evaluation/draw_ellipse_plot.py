import matplotlib.pyplot as plt
import numpy as np
import csv
from matplotlib.patches import Ellipse

def read_ellipse_data(filename, ellipse_cols=4, affordance_cols=25):
    with open(filename) as csvfile:
        print('Loading', csvfile.name)
        tsreader = csv.reader(csvfile)
        data = []
        affordances = []
        for row in tsreader:
            i = 0
            for val in row:
                if i < ellipse_cols:
                    data.append(float(val))
                else:
                    affordances.append(float(val))
                i = i + 1
        data = np.asarray(data, dtype=np.float32)
        data = data.reshape(-1, ellipse_cols)
        affordances = np.asarray(affordances, dtype=np.float32)
        affordances = affordances.reshape(-1, affordance_cols)
        return data, affordances

data, affordances = read_ellipse_data('VR_data/train-series.points')

print('Data shape: ', data.shape)
print('Affordances shape: ', affordances.shape)

# for iterator in range(2)
ells = [Ellipse(xy=[data.item((i, 0)) * 100, data.item((i, 1)) * 100],
                width=data.item((i, 2)) * 2, height=data.item((i, 3)) * 2,
                angle=0)
        for i in range(len(data))]

fig, ax = plt.subplots(subplot_kw={'aspect': 'equal'})
for e in ells:
    ax.add_artist(e)
    e.set_clip_box(ax.bbox)
    e.set_alpha(np.random.rand())
    e.set_facecolor(np.random.rand(3))

ax.set_xlim(-300, 300)
ax.set_ylim(-300, 300)

plt.show()

