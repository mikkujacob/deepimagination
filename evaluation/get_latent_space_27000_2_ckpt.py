import tensorflow as tf
import dataset_loading
import numpy as np
import matplotlib.pyplot as plt
import datetime
from tensorflow.python.tools import inspect_checkpoint as chkp
import seaborn as sb
import plot_latent_mapping_error

sb.set()

tf.reset_default_graph()


def variable_summaries(var_name, var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope(var_name + '_summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar(var_name + '_mean', mean)
        with tf.name_scope(var_name + '_stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar(var_name + '_stddev', stddev)
        tf.summary.scalar(var_name + '_max', tf.reduce_max(var))
        tf.summary.scalar(var_name + '_min', tf.reduce_min(var))
        tf.summary.histogram(var_name + '_histogram', var)


batch_size = 64
num_of_input_vectors = len(open('VR_data\\train-series-float32.timeseries', 'r').readlines())
print(num_of_input_vectors)
input_dims = 27000
cond_dims = 25
enc_in_dims = input_dims + cond_dims

vr_data = dataset_loading.read_timeseries_data_only_train('VR_data')

X_in = tf.placeholder(dtype=tf.float32, shape=[None, input_dims], name='X')
Y    = tf.placeholder(dtype=tf.float32, shape=[None, input_dims], name='Y')
Cond = tf.placeholder(dtype=tf.float32, shape=[None, cond_dims], name='Cond')
keep_prob = tf.placeholder(dtype=tf.float32, shape=(), name='keep_prob')

affordance_labels = np.asarray([[0,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
                                [2,0,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0],
                                [0,1,0,1,0,0,0,1,1,0,0,0,0,2,1,0,0,0,2,1,0,0,0,0,0],
                                [0,1,1,0,0,0,0,1,1,0,0,0,1,1,1,0,0,0,1,0,0,0,0,0,0],
                                [0,1,0,1,0,0,0,0,0,2,0,0,1,0,1,0,1,0,2,1,0,0,0,0,0],
                                [0,0,0,1,1,0,0,0,0,2,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0],
                                [0,2,0,0,0,0,0,1,1,0,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0],
                                [1,2,0,0,0,0,0,1,1,1,0,0,1,1,1,0,0,0,0,1,0,0,0,0,0],
                                [0,1,0,0,0,1,0,0,2,0,0,0,1,0,2,0,0,1,0,0,0,0,1,0,0],
                                [0,1,0,0,0,2,0,1,2,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0],
                                [0,1,0,0,0,1,0,1,0,1,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0],
                                [0,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
                                [0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,0,0,0,0,1,0,0,1,0,0],
                                [0,2,0,0,0,0,0,2,0,0,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0],
                                [0,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0],
                                [0,0,0,1,1,0,0,0,1,1,0,0,1,0,1,0,1,0,2,0,0,0,0,0,0],
                                [0,0,0,1,0,1,0,0,0,2,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0],
                                [0,0,0,0,2,0,0,0,1,1,0,1,1,0,0,0,0,0,2,0,0,0,0,0,0],
                                [0,1,0,1,0,0,0,1,1,0,0,0,1,0,1,0,1,0,2,0,0,0,0,0,0],
                                [0,2,0,0,1,0,0,0,1,1,0,0,0,0,0,1,0,0,2,0,0,0,0,0,0]],dtype=np.float32)
affordance_labels = [[(((oldValue - 0.0) * (1.0 - 0.0)) / (3 - 0)) + 0.0 for oldValue in row] for row in affordance_labels]

affordance_prop_dict = {'[0,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0]': 'Squid w Tentacles Partless OR Horseshoe',
                        '[2,0,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0]': 'Lolliop w Cube Ends Partless',
                        '[0,1,0,1,0,0,0,1,1,0,0,0,0,2,1,0,0,0,2,1,0,0,0,0,0]': 'Giant U w Flat Base Partless',
                        '[0,1,1,0,0,0,0,1,1,0,0,0,1,1,1,0,0,0,1,0,0,0,0,0,0]': 'Big Curved Axe Partless',
                        '[0,1,0,1,0,0,0,0,0,2,0,0,1,0,1,0,1,0,2,1,0,0,0,0,0]': 'Sunflower Partless',
                        '[0,0,0,1,1,0,0,0,0,2,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0]': 'Air Horn',
                        '[0,2,0,0,0,0,0,1,1,0,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0]': 'Corn Dog',
                        '[1,2,0,0,0,0,0,1,1,1,0,0,1,1,1,0,0,0,0,1,0,0,0,0,0]': 'Giant Electric Plug',
                        '[0,1,0,0,0,1,0,0,2,0,0,0,1,0,2,0,0,1,0,0,0,0,1,0,0]': 'Ring w Tail End',
                        '[0,1,0,0,0,2,0,1,2,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0]': 'Ring w Inward Spokes',
                        '[0,1,0,0,0,1,0,1,0,1,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0]': 'Large Ring w Outward Spokes',
                        '[0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,0,0,0,0,1,0,0,1,0,0]': 'Tube w Cones',
                        '[0,2,0,0,0,0,0,2,0,0,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0]': 'Thin Stick w Hammer End',
                        '[0,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0]': 'Helix',
                        '[0,0,0,1,1,0,0,0,1,1,0,0,1,0,1,0,1,0,2,0,0,0,0,0,0]': 'Giant Golf T',
                        '[0,0,0,1,0,1,0,0,0,2,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0]': 'Circle With Ring',
                        '[0,0,0,0,2,0,0,0,1,1,0,1,1,0,0,0,0,0,2,0,0,0,0,0,0]': 'Palm Tree Partless',
                        '[0,1,0,1,0,0,0,1,1,0,0,0,1,0,1,0,1,0,2,0,0,0,0,0,0]': 'Ladle',
                        '[0,2,0,0,1,0,0,0,1,1,0,0,0,0,0,1,0,0,2,0,0,0,0,0,0]': 'Plunger'}

dec_in_channels = 1
n_latent = 2

reshaped_dim = [-1, 1, 64, dec_in_channels]
inputs_decoder = round(64 * dec_in_channels / 2)


def lrelu(x, alpha=0.3):
    return tf.maximum(x, tf.multiply(x, alpha))


def encoder(X_in, keep_prob):
    activation = lrelu
    with tf.variable_scope("encoder", reuse=None):
        # print('In encoder')
        # print(tf.shape(X_in))

        # X = tf.reshape(X_in, shape=[-1, 96, 96, 1])
        
        X = tf.reshape(X_in, shape=[-1, enc_in_dims, 1])

        print('After reshape:', X.shape)
        
        # print('After reshaping with shape=[-1, 96, 96, 1]')
        # print(tf.shape(X))
        
        # x = tf.layers.conv2d(X, filters=32, kernel_size=4, strides=2, padding='same', activation=activation)
        x = tf.layers.conv1d(X, filters=32, kernel_size=4, strides=2, padding='same', activation=activation)

        print('After 1st conv1d, stride 2:', x.shape)
        
        # print('After 1st conv2d, filters=32, kernel_size=4, strides=2, padding=same, activation=lrelu')
        # print(tf.shape(x))
        
        x = tf.nn.dropout(x, keep_prob)
        # x = tf.layers.conv2d(x, filters=32, kernel_size=4, strides=2, padding='same', activation=activation)
        x = tf.layers.conv1d(x, filters=32, kernel_size=4, strides=2, padding='same', activation=activation)

        print('After 2nd conv1d, stride 2:', x.shape)
        
        # print('After dropout and 2nd conv2d, filters=32, kernel_size=4, strides=2, padding=same, activation=lrelu')
        # print(tf.shape(x))
        
        x = tf.nn.dropout(x, keep_prob)
        # x = tf.layers.conv2d(x, filters=32, kernel_size=4, strides=1, padding='same', activation=activation)
        x = tf.layers.conv1d(x, filters=32, kernel_size=4, strides=2, padding='same', activation=activation)
        
        print('After 3rd conv1d, stride 2:', x.shape)

        # print('After dropout and 3rd conv2d, filters=32, kernel_size=4, strides=1, padding=same, activation=lrelu')
        # print(tf.shape(x))
        
        x = tf.nn.dropout(x, keep_prob)
        # x = tf.layers.conv2d(x, filters=32, kernel_size=4, strides=1, padding='same', activation=activation)
        # x = tf.layers.conv1d(x, filters=32, kernel_size=4, strides=1, padding='same', activation=activation)

        # print('After 4th conv1d, stride 1:', x.shape)
        
        # x = tf.nn.dropout(x, keep_prob)
        x = tf.contrib.layers.flatten(x)
        
        # print('After dropout and flatten')
        # print(tf.shape(x))
        
        mn = tf.layers.dense(x, units=n_latent)
        sd       = 0.5 * tf.layers.dense(x, units=n_latent)            
        epsilon = tf.random_normal(tf.stack([tf.shape(x)[0], n_latent])) 
        z  = mn + tf.multiply(epsilon, tf.exp(sd))
        # print('mn')
        # print(tf.shape(mn))
        # print('sd')
        # print(tf.shape(sd))
        # print('epsilon')
        # print(tf.shape(epsilon))
        # print('z')
        # print(tf.shape(z))
        
        return z, mn, sd

def decoder(sampled_z, keep_prob):
    with tf.variable_scope("decoder", reuse=None):
        # print('In decoder')
        # print(tf.shape(sampled_z))
        
        x = tf.layers.dense(sampled_z, units=inputs_decoder, activation=lrelu)
        
        # print('After 1st dense layer, units=round(256 * dec_in_channels / 2)')
        # print(tf.shape(x))
        
        x = tf.layers.dense(x, units=inputs_decoder * 2, activation=lrelu)
        
        # print('After 2nd dense layer, units=round(256 * dec_in_channels / 2)')
        # print(tf.shape(x))

        x = tf.reshape(x, reshaped_dim)

        print('After reshape:', x.shape)
        
        # print('After reshaping with shape=[-1, 16, 16, 1]')
        # print(tf.shape(x))
        
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=[1,4], strides=[1,2], padding='same', activation=tf.nn.relu)
        
        # print('After 1st conv2d_transpose, filters=32, kernel_size=4, strides=2, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))

        print('After 1st conv2d_transpose, stride 2:', x.shape)
        
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=[1,4], strides=[1,2], padding='same', activation=tf.nn.relu)
        
        # print('After dropout and 2nd conv2d_transpose, filters=32, kernel_size=4, strides=1, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))

        print('After 2nd conv2d_transpose, stride 2:', x.shape)
        
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=[1,4], strides=[1,2], padding='same', activation=tf.nn.relu)

        # print('After dropout and 3rd conv2d_transpose, filters=32, kernel_size=4, strides=1, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))

        print('After 3rd conv2d_transpose, stride 2:', x.shape)
        
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=[1,4], strides=[1,2], padding='same', activation=tf.nn.relu)

        # print('After dropout and 3rd conv2d_transpose, filters=32, kernel_size=4, strides=1, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))

        print('After 4th conv2d_transpose, stride 2:', x.shape)
        
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=[1,4], strides=[1,2], padding='same', activation=tf.nn.relu)

        # print('After dropout and 3rd conv2d_transpose, filters=32, kernel_size=4, strides=1, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))

        print('After 5th conv2d_transpose, stride 2:', x.shape)
        
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=2, kernel_size=[1,4], strides=[1,2], padding='same', activation=tf.nn.relu)

        # print('After dropout and 3rd conv2d_transpose, filters=32, kernel_size=4, strides=1, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))

        print('After 6th conv2d_transpose, stride 2:', x.shape)
        
        x = tf.nn.dropout(x, keep_prob)

        x = tf.contrib.layers.flatten(x)
        
        # print('After flatten')
        # print(tf.shape(x))
        
        x = tf.layers.dense(x, units=input_dims, activation=tf.nn.sigmoid)
        
        # print('After dense layer, units=96*96, activation=tf.nn.sigmoid')
        # print(tf.shape(x))

        print('After dense:', x.shape)
        
        img = tf.reshape(x, shape=[-1, input_dims])
        
        # print('After reshaping with shape=[-1, 96, 96]')
        # print(tf.shape(img))
        return img

XCond = tf.concat([X_in, Cond], 1)

sampled, mn, sd = encoder(XCond, keep_prob)
sample_cond = tf.concat([sampled, Cond], 1)
dec = decoder(sample_cond, keep_prob)

img_loss = tf.reduce_sum(tf.squared_difference(dec, Y), 1)
latent_loss = -0.5 * tf.reduce_sum(1.0 + 2.0 * sd - tf.square(mn) - tf.exp(2.0 * sd), 1)
loss = tf.reduce_mean(img_loss + latent_loss)
optimizer = tf.train.AdamOptimizer(0.0005).minimize(loss)

variable_summaries('img_loss', img_loss)
variable_summaries('latent_loss', latent_loss)
tf.summary.scalar('mean_loss', loss)

merged = tf.summary.merge_all()

#init_op = tf.global_variables_initializer()
saver = tf.train.Saver(max_to_keep=2)

# fhInputFile = open('outputs/' + "input-vectors" + '.csv', 'w')
fhOutputFile = open('outputs/' + "latent-space-vectors" + '.csv', 'w')

with tf.Session() as sess:
    # restore latest checkpoint
    latest_checkpoint_path = tf.train.latest_checkpoint('logged_outputs/outputs - 09212018')
    print("LATESTCHKPT:" + latest_checkpoint_path)
    saver.restore(sess, latest_checkpoint_path)

    iter_start = 0
    n_iters = int(np.ceil(num_of_input_vectors / batch_size))

    for i in range(iter_start, n_iters):
        batch, batch_labels = vr_data.train.next_timeseries_batch(batch_size=batch_size, shuffle=False, looping=False)
        batch = np.reshape(batch, [-1, input_dims])
        batch_labels = np.reshape(batch_labels, [-1, cond_dims])
        z, mu, sigm = sess.run([sampled, mn, sd], feed_dict={X_in: batch, Cond: batch_labels, Y: batch, keep_prob: 1.0})
        # print(str(i) + " : batch length " + str(len(batch)))
        # print(str(i) + " : z length " + str(len(z)))
        # np.savetxt(fhInputFile, batch, delimiter=',')
        np.savetxt(fhOutputFile, z, delimiter=',')

# fhInputFile.close()
fhOutputFile.close()

plot_latent_mapping_error.plot_latent_error('outputs/' + "latent-space-vectors" + '.csv')