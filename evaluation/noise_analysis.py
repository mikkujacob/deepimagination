import tensorflow as tf
from tensorflow.keras.models import model_from_json
from tensorflow.keras import backend as K
import numpy as np
import os, sys

root_dir = os.path.join('.', '..')
outputs_dir = 'outputs'
training_dir = 'training'
vr_data_dir = 'VR_data'
checkpoint_dir = os.path.join(root_dir, outputs_dir) 

sys.path.insert(0, os.path.join(root_dir, training_dir))
import dataset_loading

n_latent = 2

cvae_path = os.path.join(checkpoint_dir, 'affordance-cvae-27000-2-final.ckpt')
cvae_json_path = os.path.join(checkpoint_dir, 'cvae.json')

cvae = model_from_json(open(cvae_json_path).read())
cvae.load_weights(cvae_path)

vr_data = dataset_loading.read_timeseries_data_only_train(os.path.join(root_dir, vr_data_dir))

X = vr_data.train.images.squeeze()
y = vr_data.train.labels.squeeze()

def img_loss(y_true, y_pred): # Reconstruction, mean_squared_error loss
    return np.sum(np.square(y_pred - y_true), axis=1)

baseline_v = cvae.predict([X, y], batch_size=64) # (?, 27000) shape, baseline output gesture (no noise)
no_noise_error = img_loss(X, baseline_v) # (?,) shape, mean squared error between true gesture and baseline gesture
true_error = np.zeros_like(no_noise_error)
baseline_error = np.zeros_like(no_noise_error)

# Add random Gaussian noise 10 times, accumulate errors to find average:
for i in range(10):
  print("Adding Gaussian noise...iteration %d" % (i + 1))
  noise = np.random.normal(scale=0.05, size=y.shape)
  noisy_v = cvae.predict([X, np.clip(y + noise, 0, 1)], batch_size=64) # (?, 27000) shape, noisy output gesture
  true_error += img_loss(X, noisy_v) # Error when compared to true gesture
  baseline_error += img_loss(baseline_v, noisy_v) # Error when compared to baseline output gesture

true_error /= 10
baseline_error /= 10
print("no_noise_error: %.4f, true_noise_error: %.4f, baseline_noise_error: %.4f" % (np.mean(no_noise_error), np.mean(true_error), np.mean(baseline_error)))
