import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb

def plot_latent_error(latent_file_name):
	# open files with latent vectors and distancematrix
	fh_latent_vectors = open(latent_file_name, 'r')
	fh_original_distance_matrix = open('..\\gestureanalysis\\data\\VRData\\New_Data_Annotated\\GestureDTWDistanceMatrix-900-3.33-10.0-2018-09-21-15-43-02-126.matrix')

	# read both in as strings
	vectors_as_strings = fh_latent_vectors.readlines()
	true_rows_as_strings = fh_original_distance_matrix.readlines()

	# check the number of vectors in both are the same
	print("Number of latent space vectors and original vectors is the same: " + str(len(vectors_as_strings) == len(true_rows_as_strings)))
	# print("Number of vectors in latent space: " + str(len(vectors_as_strings)))
	# print("Number of vectors in original space: " + str(len(true_rows_as_strings)))
	num_of_input_vectors = len(vectors_as_strings)

	# make arrays of zeroes to store distances
	latent_space_distance_matrix = np.zeros((num_of_input_vectors, num_of_input_vectors))
	true_distance_matrix = np.zeros((num_of_input_vectors, num_of_input_vectors))

	# fill in latent space distance matrix
	for i in range(0, num_of_input_vectors):
		vector1 = vectors_as_strings[i]
		vx1, vy1 = vector1.split(',')
		vx1 = float(vx1)
		vy1 = float(vy1)
		for j in range(0, i+1):
			vector2 = vectors_as_strings[j]
			vx2, vy2 = vector2.split(',')
			vx2 = float(vx2)
			vy2 = float(vy2)
			distance = np.sqrt((vx2-vx1)**2 + (vy2-vy1)**2)
			latent_space_distance_matrix[i][j] = distance

	# fill in true distance matrix
	for i in range(len(true_rows_as_strings)):
		true_distance_row = true_rows_as_strings[i].split(',')
		true_distance_row = np.array(list(map(float, true_distance_row)))
		true_distance_matrix[i] = true_distance_row

	# standardize all distances against max distance in the true distance array
	temp_true_dist_matrix = np.reshape(true_distance_matrix, [1, -1])
	max_true_distance_loc = np.argmax(temp_true_dist_matrix, axis=1)
	standard_true_distance_matrix = np.true_divide(true_distance_matrix, temp_true_dist_matrix[0, max_true_distance_loc])

	temp_latent_dist_matrix = np.reshape(latent_space_distance_matrix, [1, -1])
	max_latent_distance = temp_latent_dist_matrix[0, max_true_distance_loc]
	standard_latent_distance_matrix = np.true_divide(latent_space_distance_matrix, max_latent_distance)

	# normalize the error matrix
	error_matrix = np.subtract(standard_true_distance_matrix, standard_latent_distance_matrix)
	abs_error_matrix = np.absolute(error_matrix)
	max_abs_error = np.amax(abs_error_matrix)
	normalized_error_matrix = np.true_divide(error_matrix, max_abs_error)

	# plot on a heatmap
	sb.heatmap(error_matrix, cmap="YlGnBu")
	plt.show()
			
	fh_latent_vectors.close()
	fh_latent_vectors.close()