import tensorflow as tf

graph_path = 'outputs'
saver = tf.train.import_meta_graph(graph_path + "/affordance-cvae-27000-2-final.ckpt.meta")
graph = tf.get_default_graph()
input_graph_def = graph.as_graph_def()
sess = tf.Session()
saver.restore(sess, graph_path + "/affordance-cvae-27000-2-final.ckpt")

for op in graph.get_operations():
    print(op.name)

output_node_names = []
output_node_names.append("decoder/Reshape_1")  # Specify the real node name
output_node_names.append("encoder/dense/BiasAdd") # Specify the real node name
output_node_names.append("encoder/dense_1/BiasAdd") # Specify the real node name
output_node_names.append("encoder/add") # Specify the real node name
output_graph_def = tf.graph_util.convert_variables_to_constants(
    sess,
    input_graph_def,
    output_node_names
)

#for op in graph.get_operations():
#    print(op.name)

output_file = graph_path + "/affordance-cvae-27000-2-final.ckpt.bytes"
with tf.gfile.GFile(output_file, "wb") as f:
    f.write(output_graph_def.SerializeToString())
    
sess.close()