import tensorflow as tf
    
saver = tf.train.import_meta_graph("outputs/affordance-cvae-final.ckpt.meta")
graph = tf.get_default_graph()
input_graph_def = graph.as_graph_def()
sess = tf.Session()
saver.restore(sess, "outputs/affordance-cvae-final.ckpt")

for op in graph.get_operations():
    print(op.name)

output_node_names = []
output_node_names.append("decoder/Reshape_1")  # Specify the real node name
output_graph_def = tf.graph_util.convert_variables_to_constants(
    sess,
    input_graph_def,
    output_node_names
)

for op in graph.get_operations():
    print(op.name)

output_file = "outputs/affordance-cvae-final.ckpt.pb"
with tf.gfile.GFile(output_file, "wb") as f:
    f.write(output_graph_def.SerializeToString())
    
sess.close()