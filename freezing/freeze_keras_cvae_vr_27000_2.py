import tensorflow as tf
from tensorflow.python.framework import graph_io, graph_util
from tensorflow.keras.models import model_from_json
from tensorflow.keras import backend as K
import os

root_dir = os.path.join('.', '..')
outputs_dir = 'outputs'
n_latent = 2
output_node_names = ['model_1/reshape_2/Reshape', 'dense_5/BiasAdd', 'dense_1_1/BiasAdd']


checkpoint_dir = os.path.join(root_dir, outputs_dir)
cvae_path = os.path.join(checkpoint_dir, 'affordance-cvae-27000-2-final.ckpt')
cvae_json_path = os.path.join(checkpoint_dir, 'cvae.json')
encoder_json_path = os.path.join(checkpoint_dir, 'encoder.json')
decoder_json_path = os.path.join(checkpoint_dir, 'decoder.json')


cvae = model_from_json(open(cvae_json_path).read())
encoder = model_from_json(open(encoder_json_path).read())
decoder = model_from_json(open(decoder_json_path).read())

cvae.load_weights(cvae_path)

# print([node.op.name for node in cvae.outputs])
# print([node.op.name for node in encoder.outputs])
# print([node.op.name for node in decoder.outputs])

sess = K.get_session()

constant_graph = graph_util.convert_variables_to_constants(
    sess,
    sess.graph.as_graph_def(),
    output_node_names)

graph_io.write_graph(constant_graph, checkpoint_dir, 'affordance-cvae-27000-2-final.ckpt.bytes',
                         as_text=False)
print('Saved the freezed graph at %s' % os.path.join(checkpoint_dir, 'affordance-cvae-27000-2-final.ckpt.bytes'))

# Helpful code to print names of all ops of the frozen graph:
# def load_graph(model_path):
#     # We load the protobuf file from the disk and parse it to retrieve the 
#     # unserialized graph_def
#     with tf.gfile.GFile(model_path, "rb") as f:
#         graph_def = tf.GraphDef()
#         graph_def.ParseFromString(f.read())

#     # Then, we import the graph_def into a new Graph and returns it 
#     with tf.Graph().as_default() as graph:
#         # The name var will prefix every op/nodes in your graph
#         # Since we load everything in a new graph, this is not needed
#         tf.import_graph_def(graph_def, name="prefix")
#     return graph

# graph = load_graph('affordance-cvae-27000-2-final.ckpt.bytes')

# for op in graph.get_operations():
#     print(op.name)
