import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import datetime

# from tensorflow.examples.tutorials.mnist import input_data
import dataset_loading

tf.reset_default_graph()

def variable_summaries(var_name, var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope(var_name + '_summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar(var_name + '_mean', mean)
        with tf.name_scope(var_name + '_stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar(var_name + '_stddev', stddev)
        tf.summary.scalar(var_name + '_max', tf.reduce_max(var))
        tf.summary.scalar(var_name + '_min', tf.reduce_min(var))
        tf.summary.histogram(var_name + '_histogram', var)

batch_size = 64

input_dims = 16000
cond_dims = 25
enc_in_dims = input_dims + cond_dims

X_in = tf.placeholder(dtype=tf.float32, shape=[None, input_dims], name='X')
Y    = tf.placeholder(dtype=tf.float32, shape=[None, input_dims], name='Y')
# Y_flat = tf.reshape(Y, shape=[-1, 96 * 96])
Cond = tf.placeholder(dtype=tf.float32, shape=[None, cond_dims], name='Cond')
keep_prob = tf.placeholder(dtype=tf.float32, shape=(), name='keep_prob')
is_training = tf.placeholder(dtype=tf.bool, name="is_training")


affordance_labels = np.asarray([[0,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
                                [2,0,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0],
                                [0,1,0,1,0,0,0,1,1,0,0,0,0,2,1,0,0,0,2,1,0,0,0,0,0],
                                [0,1,1,0,0,0,0,1,1,0,0,0,1,1,1,0,0,0,1,0,0,0,0,0,0],
                                [0,1,0,1,0,0,0,0,0,2,0,0,1,0,1,0,1,0,2,1,0,0,0,0,0],
                                [0,0,0,1,1,0,0,0,0,2,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0],
                                [0,2,0,0,0,0,0,1,1,0,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0],
                                [1,2,0,0,0,0,0,1,1,1,0,0,1,1,1,0,0,0,0,1,0,0,0,0,0],
                                [0,1,0,0,0,1,0,0,2,0,0,0,1,0,2,0,0,1,0,0,0,0,1,0,0],
                                [0,1,0,0,0,2,0,1,2,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0],
                                [0,1,0,0,0,1,0,1,0,1,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0],
                                [0,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
                                [0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,0,0,0,0,1,0,0,1,0,0],
                                [0,2,0,0,0,0,0,2,0,0,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0],
                                [0,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0],
                                [0,0,0,1,1,0,0,0,1,1,0,0,1,0,1,0,1,0,2,0,0,0,0,0,0],
                                [0,0,0,1,0,1,0,0,0,2,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0],
                                [0,0,0,0,2,0,0,0,1,1,0,1,1,0,0,0,0,0,2,0,0,0,0,0,0],
                                [0,1,0,1,0,0,0,1,1,0,0,0,1,0,1,0,1,0,2,0,0,0,0,0,0],
                                [0,2,0,0,1,0,0,0,1,1,0,0,0,0,0,1,0,0,2,0,0,0,0,0,0]],dtype=np.float32)
affordance_labels = [[(((oldValue - 0.0) * (1.0 - 0.0)) / (3 - 0)) + 0.0 for oldValue in row] for row in affordance_labels]

affordance_prop_dict = {'[0,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0]': 'Squid w Tentacles Partless OR Horseshoe',
                        '[2,0,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0]': 'Lolliop w Cube Ends Partless',
                        '[0,1,0,1,0,0,0,1,1,0,0,0,0,2,1,0,0,0,2,1,0,0,0,0,0]': 'Giant U w Flat Base Partless',
                        '[0,1,1,0,0,0,0,1,1,0,0,0,1,1,1,0,0,0,1,0,0,0,0,0,0]': 'Big Curved Axe Partless',
                        '[0,1,0,1,0,0,0,0,0,2,0,0,1,0,1,0,1,0,2,1,0,0,0,0,0]': 'Sunflower Partless',
                        '[0,0,0,1,1,0,0,0,0,2,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0]': 'Air Horn',
                        '[0,2,0,0,0,0,0,1,1,0,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0]': 'Corn Dog',
                        '[1,2,0,0,0,0,0,1,1,1,0,0,1,1,1,0,0,0,0,1,0,0,0,0,0]': 'Giant Electric Plug',
                        '[0,1,0,0,0,1,0,0,2,0,0,0,1,0,2,0,0,1,0,0,0,0,1,0,0]': 'Ring w Tail End',
                        '[0,1,0,0,0,2,0,1,2,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0]': 'Ring w Inward Spokes',
                        '[0,1,0,0,0,1,0,1,0,1,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0]': 'Large Ring w Outward Spokes',
                        '[0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,0,0,0,0,1,0,0,1,0,0]': 'Tube w Cones',
                        '[0,2,0,0,0,0,0,2,0,0,0,1,0,1,0,0,0,0,2,0,0,0,0,0,0]': 'Thin Stick w Hammer End',
                        '[0,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0]': 'Helix',
                        '[0,0,0,1,1,0,0,0,1,1,0,0,1,0,1,0,1,0,2,0,0,0,0,0,0]': 'Giant Golf T',
                        '[0,0,0,1,0,1,0,0,0,2,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0]': 'Circle With Ring',
                        '[0,0,0,0,2,0,0,0,1,1,0,1,1,0,0,0,0,0,2,0,0,0,0,0,0]': 'Palm Tree Partless',
                        '[0,1,0,1,0,0,0,1,1,0,0,0,1,0,1,0,1,0,2,0,0,0,0,0,0]': 'Ladle',
                        '[0,2,0,0,1,0,0,0,1,1,0,0,0,0,0,1,0,0,2,0,0,0,0,0,0]': 'Plunger'}

dec_in_channels = 1
n_latent = 8

reshaped_dim = [-1, 16, 16, dec_in_channels]
inputs_decoder = round(256 * dec_in_channels / 2)


def lrelu(x, alpha=0.3):
    return tf.maximum(x, tf.multiply(x, alpha))

def encoder(X_in, keep_prob, is_training):
    activation = lrelu
    with tf.variable_scope("encoder", reuse=None):
        X = tf.reshape(X_in, shape=[-1, enc_in_dims, 1])
        print('Shape after reshape: ' + str(X.shape))
        x = tf.layers.conv1d(X, filters=32, kernel_size=4, strides=2, padding='same', activation=activation)
        print('Shape after conv1d: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv1d(x, filters=32, kernel_size=4, strides=1, padding='same', activation=activation)
        print('Shape after conv1d: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv1d(x, filters=32, kernel_size=4, strides=1, padding='same', activation=activation)
        print('Shape after conv1d: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv1d(x, filters=32, kernel_size=4, strides=1, padding='same', activation=activation)
        print('Shape after conv1d: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.flatten(x)
        print('Shape after flatten: ' + str(x.shape))
        
        mn = tf.layers.dense(x, units=n_latent)
        sd       = 0.5 * tf.layers.dense(x, units=n_latent)            
        epsilon = tf.random_normal(tf.stack([tf.shape(x)[0], n_latent])) 
        z  = mn + tf.multiply(epsilon, tf.exp(sd))
        print('Shape after dense: ' + str(z.shape))
        
        return z, mn, sd

def decoder(sampled_z, keep_prob, is_training):
    with tf.variable_scope("decoder", reuse=None):
        x = tf.layers.dense(sampled_z, units=inputs_decoder, activation=lrelu)
        # x = tf.layers.dense(x, units=inputs_decoder * 2, activation=lrelu)
        x = tf.layers.dense(x, units=inputs_decoder * 2, activation=lrelu)
        x = tf.reshape(x, reshaped_dim)
        print('Shape after reshape: ' + str(x.shape))
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=1, padding='same', activation=lrelu)
        print('Shape after conv2d_transpose: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=1, padding='same', activation=lrelu)
        print('Shape after conv2d_transpose: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=1, padding='same', activation=lrelu)
        print('Shape after conv2d_transpose: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=1, padding='same', activation=lrelu)
        print('Shape after conv2d_transpose: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=1, padding='same', activation=lrelu)
        print('Shape after conv2d_transpose: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=2, padding='same', activation=lrelu)
        print('Shape after conv2d_transpose: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=16, kernel_size=4, strides=2, padding='same', activation=lrelu)
        print('Shape after conv2d_transpose: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=1, kernel_size=4, strides=2, padding='same', activation=lrelu)
        print('Shape after conv2d_transpose: ' + str(x.shape))
        x = tf.layers.batch_normalization(x, training=is_training)
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.flatten(x)
        print('Shape after flatten: ' + str(x.shape))
        # x = tf.layers.dense(x, units=input_dims / 4, activation=lrelu)
        # print('Shape after dense: ' + str(x.shape))
        # x = tf.layers.dense(x, units=input_dims / 2, activation=lrelu)
        # print('Shape after dense: ' + str(x.shape))
        x = tf.layers.dense(x, units=input_dims, activation=tf.nn.sigmoid)
        print('Shape after dense: ' + str(x.shape))
        
        img = tf.reshape(x, shape=[-1, input_dims])
        
        return img

XCond = tf.concat([X_in, Cond], 1)

# sampled, mn, sd = encoder(X_in, keep_prob)
sampled, mn, sd = encoder(XCond, keep_prob, is_training)
sample_cond = tf.concat([sampled, Cond], 1)
# dec = decoder(sampled, keep_prob)
dec = decoder(sample_cond, keep_prob, is_training)

# unreshaped = tf.reshape(dec, [-1, 96*96])
# print('Decoder output after reshaping with shape=[-1, 96, 96]')
# print(tf.shape(unreshaped))
img_loss = tf.reduce_sum(tf.squared_difference(dec, Y), 1)
# print('Image loss')
# print(tf.shape(img_loss))
latent_loss = -0.5 * tf.reduce_sum(1.0 + 2.0 * sd - tf.square(mn) - tf.exp(2.0 * sd), 1)
# print('Latent loss')
# print(tf.shape(latent_loss))
loss = tf.reduce_mean(img_loss + latent_loss)
# print('Total loss')
# print(tf.shape(loss))
extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
with tf.control_dependencies(extra_update_ops):
    optimizer = tf.train.AdamOptimizer(0.00025).minimize(loss)

# variable_summaries('img_loss', img_loss)
# variable_summaries('latent_loss', latent_loss)
tf.summary.scalar('mean_loss', loss)

merged = tf.summary.merge_all()

saver = tf.train.Saver(max_to_keep=2)

run_tensorboard = False
n_iters = 40000
with tf.Session() as sess:
    if run_tensorboard == True:
        train_writer = tf.summary.FileWriter('logs/train/event-' + str(datetime.datetime.now()).replace(' ', '-').replace(':', '-') + '/', sess.graph)
        print('Tensorboard started')
    
    latest_checkpoint_path = tf.train.latest_checkpoint('outputs/')
    print(latest_checkpoint_path)
    iter_start = 0
    if latest_checkpoint_path != None:
        saver.restore(sess, latest_checkpoint_path)
        iter_start = int(str(latest_checkpoint_path).split('/')[1].split('-of-')[0])
    else:
        sess.run(tf.global_variables_initializer())

    # mnist = input_data.read_data_sets('MNIST_data')
    vr_data = dataset_loading.read_timeseries_data_only_train('./../VR_data')

    print(str(datetime.datetime.now()))
    for i in range(iter_start, n_iters):
        # batch = [np.reshape(b, [180, 150]) for b in vr_data.train.next_timeseries_batch(batch_size=batch_size)[0]]
        batch, batch_labels = vr_data.train.next_timeseries_batch(batch_size=batch_size)
        batch = np.reshape(batch, [-1, input_dims])
        batch_labels = np.reshape(batch_labels, [-1, cond_dims])
        # batchprint = np.asarray(batch)
        # print('Batch after reshaping with shape=b, [180, 150]')
        # print(tf.shape(batchprint))
        # print('Batch')
        # print(batchprint)
        sess.run(optimizer, feed_dict = {X_in: batch, Cond: batch_labels, Y: batch, keep_prob: 0.8, is_training: True})
            
        if not i % 50:
            ls, d, i_ls, d_ls, mu, sigm = sess.run([loss, dec, img_loss, latent_loss, mn, sd], feed_dict = {X_in: batch, Cond: batch_labels, Y: batch, keep_prob: 1.0, is_training: False})
            # plt.imshow(np.reshape(batch[0], [180, 150]), cmap='gray')
            if np.isnan(ls):
                print('Loss was NaN, so resetting session to previous checkpoint or new session.')
                latest_checkpoint_path = tf.train.latest_checkpoint('outputs/')
                print(latest_checkpoint_path)
                iter_start = 0
                if latest_checkpoint_path != None:
                    saver.restore(sess, latest_checkpoint_path)
                    iter_start = int(str(latest_checkpoint_path).split('/')[1].split('-of-')[0])
                else:
                    sess.run(tf.global_variables_initializer())
                break
            
            # batch_labels_remapped = [int(((oldValue - 0.0) * (3.0 - 0.0)) / (1.0 - 0.0)) for oldValue in batch_labels[0]]
            # plt.savefig('outputs/'+ str(i) + '-' + affordance_prop_dict[str(batch_labels_remapped).replace(' ', '')] +'-original.png', bbox_inches='tight')
            # print('Original image after reshape with [batch[0], [180, 150]]')
            # print(np.reshape(batch[0], [180, 150]).shape)
            # print('Original image after reshape with [batch[0], [180, 150]]')
            # print(np.reshape(batch[0], [180, 150]))
            # with open('outputs/' + str(i) + '-' + affordance_prop_dict[str(batch_labels_remapped).replace(' ', '')] + '-original.csv', 'w') as csvfile:
            #     np.savetxt(csvfile, np.reshape(batch[0], [input_dims]), delimiter=',')
            # print(d.shape)
            # dimg = np.reshape(d[0], [180, 150])
            # print(dimg.shape)
            # print("Image:", dimg)
            # plt.imshow(dimg, cmap='gray')
            # plt.savefig('outputs/'+ str(i) + '-' + affordance_prop_dict[str(batch_labels_remapped).replace(' ', '')] +'-cvae.png', bbox_inches='tight')
            # print('Generated image')
            # print(tf.shape(d[0]))
            # print('Generated image')
            # print(d[0])
            # with open('outputs/' + str(i) + '-' + affordance_prop_dict[str(batch_labels_remapped).replace(' ', '')] + '-cvae.csv', 'w') as csvfile:
            #     np.savetxt(csvfile, np.reshape(d[0], [input_dims]), delimiter=',')
            # print('Image loss')
            # print(i_ls)
            print(i, ls, np.mean(i_ls), np.mean(d_ls))

        if not i % 100 and i > 0:
            # n_randoms = 10
            # randoms = np.asarray([np.random.normal(0, 1, n_latent) for _ in range(n_randoms)])
            # print(randoms.shape)
            # conds_indices = [np.random.randint(len(affordance_labels)) for _ in range(n_randoms)]
            # conds = np.asarray([affordance_labels[i] for i in conds_indices])
            # print(conds.shape)
            # randoms_cond = np.concatenate([randoms, conds], 1)

            # randomprint = np.asarray(randoms)
            # print('Random samples')
            # print(tf.shape(randomprint))
            # print('Random samples')
            # print(randomprint)
            # imgs = sess.run(dec, feed_dict = {sampled: randoms, keep_prob: 1.0})
            # imgs = sess.run(dec, feed_dict = {sample_cond: randoms_cond, keep_prob: 1.0})
            
            if run_tensorboard == True:
                _, _, _, _, _, _, summary = sess.run([loss, dec, img_loss, latent_loss, mn, sd, merged], feed_dict = {X_in: batch, Cond: batch_labels, Y: batch, keep_prob: 1.0, is_training: False})
                train_writer.add_summary(summary, i)
                print('Tensorboard summary written')
            
            # imgs = [np.reshape(imgs[i], [180, 150]) for i in range(len(imgs))]
            # imgsprint = np.asarray(imgs)
            # print('Images')
            # print(tf.shape(imgsprint))
            # j = 0
            # print(len(imgs))
            # for img in imgs:
            #     # imgprint = np.reshape(np.asarray(img), [180, 150])
            #     conds_remapped = [int(((oldValue - 0.0) * (3.0 - 0.0)) / (1.0 - 0.0)) for oldValue in conds[j]]
            #     # print('Image')
            #     # print(tf.shape(imgprint))
            #     with open('outputs/' + str(i) + '-cvae-generated-'+ str(j) + '-' + affordance_prop_dict[str(conds_remapped).replace(' ', '')] +'.csv', 'w') as csvfile:
            #         np.savetxt(csvfile, np.reshape(np.asarray(img), [input_dims]), delimiter=',')
            #     # plt.figure(figsize=(1,1))
            #     # plt.axis('off')
            #     # plt.imshow(img, cmap='gray')
            #     # plt.savefig('outputs/'+ str(i) +'-cvae-generated-'+ str(j) + '-' + affordance_prop_dict[str(conds_remapped).replace(' ', '')] +'.png', bbox_inches='tight')
            #     j += 1
        
        if not i % 1000 and i > 0:
            save_path = saver.save(sess, 'outputs/' + str(i+1) + '-of-' + str(n_iters) + 'affordance-cvae' + '.ckpt')
            print('Saved model to ', save_path)

    print(str(datetime.datetime.now()))

    # n_randoms = 1000
    # randoms = np.asarray([np.random.normal(0, 1, n_latent) for _ in range(n_randoms)])
    # # print(randoms.shape)
    # conds_indices = [np.random.randint(len(affordance_labels)) for _ in range(n_randoms)]
    # conds = np.asarray([affordance_labels[i] for i in conds_indices])
    # # print(conds.shape)
    # randoms_cond = np.concatenate([randoms, conds], 1)
    # # randoms = [np.random.normal(0, 1, n_latent) for _ in range(100)]
    # # imgs = sess.run(dec, feed_dict = {sampled: randoms, keep_prob: 1.0})
    # imgs = sess.run(dec, feed_dict = {sample_cond: randoms_cond, keep_prob: 1.0})
    # imgs = [np.reshape(np.asarray(img), [input_dims]) for i in range(len(imgs))]

    # j = 0
    # for img in imgs:
    #     # plt.figure(figsize=(1,1))
    #     # plt.axis('off')
    #     # imgprint = np.reshape(np.asarray(img), [180, 150])
    #     conds_remapped = [int(((oldValue - 0.0) * (3.0 - 0.0)) / (1.0 - 0.0)) for oldValue in conds[j]]
    #     with open('outputs/final-cvae-generated-' + str(j) + '-' + affordance_prop_dict[str(conds_remapped).replace(' ', '')] + '.csv', 'w') as csvfile:
    #         np.savetxt(csvfile, img, delimiter=',')
    #     # plt.imshow(img, cmap='gray')
    #     # plt.savefig('outputs/final-cvae-generated-' + str(j) + '-' + affordance_prop_dict[str(conds_remapped).replace(' ', '')] + '.png', bbox_inches='tight')
    #     j += 1

    saver.save(sess, 'outputs/'+ 'affordance-cvae-' + str(input_dims) + '-' + str(n_latent) + '-final.ckpt')

    print(str(datetime.datetime.now()))
