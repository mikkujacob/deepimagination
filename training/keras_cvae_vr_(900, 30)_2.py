import tensorflow as tf
from tensorflow.keras import layers, callbacks
from tensorflow.keras.models import Model, load_model
import numpy as np
import os, sys, pickle
import matplotlib.pyplot as plt

import dataset_loading

# Custom callback to manage checkpoint files
class TrainingManager(callbacks.Callback):
    def on_epoch_end(self, epoch, logs):
        # Print metric values for the first, last and every 10th epoch
        if epoch == initial_epoch or (epoch + 1) % 10 == 0 or (epoch + 1) == n_epochs:
            print('\nEpoch: %04d, ' % (epoch + 1), ', '.join('%s: %.4f' % (key, val) for key, val in sorted(logs.items())))
        # Delete old checkpoints - only keep `max_to_keep` recent checkpoints
        if (epoch + 1) % period == 0 and epoch >= max_to_keep * period:
            tf.train.remove_checkpoint(os.path.join(checkpoint_dir, '%04d-of-%04d-affordance-cvae.ckpt' % (epoch + 1 - max_to_keep * period, n_epochs)))
        if (epoch + 1) == n_epochs:
            # Rename the final checkpoint file to match behavior of non-keras based code
            if tf.train.checkpoint_exists(os.path.join(checkpoint_dir, '%04d-of-%04d-affordance-cvae.ckpt' % (n_epochs, n_epochs))):
                tf.train.remove_checkpoint(os.path.join(checkpoint_dir, '%04d-of-%04d-affordance-cvae.ckpt' % (n_epochs, n_epochs)))
            self.model.save_weights(os.path.join(checkpoint_dir, 'affordance-cvae-%d-%d-final.ckpt' % (input_dims, n_latent)))

def encoder(X_in, rate):
    print('Reshaped input:', X_in.shape)

    x = layers.Conv1D(filters=32, kernel_size=4, strides=2, padding='same')(X_in)
    x = layers.LeakyReLU()(x)

    print('After 1st conv1d, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)
    x = layers.Conv1D(filters=32, kernel_size=4, strides=2, padding='same')(x)
    x = layers.LeakyReLU()(x)

    print('After 2nd conv1d, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)
    x = layers.Conv1D(filters=32, kernel_size=4, strides=2, padding='same')(x)
    x = layers.LeakyReLU()(x)
    
    print('After 3rd conv1d, stride 2:', x.shape)

    x = layers.Dropout(rate)(x)
    
    x = layers.Flatten()(x)

    z_mu = layers.Dense(units=n_latent)(x)
    # Don't multiply by 0.5 - use as is
    z_log_var = layers.Dense(units=n_latent)(x)

    # Wrap tf operations in Lambda layers
    z_sigma = layers.Lambda(lambda t: tf.exp(.5 * t))(z_log_var)
    eps = layers.Lambda(lambda t: tf.random_normal(shape=(tf.shape(t)[0], n_latent)))(x)

    z_eps = layers.Multiply()([z_sigma, eps])
    z = layers.Add()([z_mu, z_eps])

    return z, z_mu, z_log_var

def decoder(sampled_z, rate):
    x = layers.Dense(units=inputs_decoder)(sampled_z)
    x = layers.LeakyReLU()(x)

    x = layers.Dense(units=inputs_decoder * 2)(x)
    x = layers.LeakyReLU()(x)
    
    x = layers.Reshape(reshaped_dim)(x)

    print('After reshape:', x.shape)
    
    x = layers.Conv2DTranspose(filters=256, kernel_size=[1,4], strides=[1,2], padding='same', activation='relu')(x)
    
    print('After 1st conv2d_transpose, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)
    x = layers.Conv2DTranspose(filters=256, kernel_size=[1,4], strides=[1,2], padding='same', activation='relu')(x)
    
    print('After 2nd conv2d_transpose, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)
    x = layers.Conv2DTranspose(filters=256, kernel_size=[1,4], strides=[1,2], padding='same', activation='relu')(x)

    print('After 3rd conv2d_transpose, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)
    x = layers.Conv2DTranspose(filters=256, kernel_size=[1,4], strides=[1,2], padding='same', activation='relu')(x)

    print('After 4th conv2d_transpose, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)
    x = layers.Conv2DTranspose(filters=256, kernel_size=[1,4], strides=[1,2], padding='same', activation='relu')(x)

    print('After 5th conv2d_transpose, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)
    x = layers.Conv2DTranspose(filters=128, kernel_size=[1,4], strides=[1,2], padding='same', activation='relu')(x)

    print('After 6th conv2d_transpose, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)
    x = layers.Conv2DTranspose(filters=1, kernel_size=[1,4], strides=[1,2], padding='same', activation='relu')(x)

    print('After 7th conv2d_transpose, stride 2:', x.shape)
    
    x = layers.Dropout(rate)(x)

    x = layers.Flatten()(x)
    
    x = layers.Dense(units=input_dims, activation='sigmoid')(x)
    
    print('After dense:', x.shape)
    
    return x

# Define metrics:
def cvae_loss(y_true, y_pred):
    return img_loss(y_true, y_pred) + latent_loss()

def latent_loss(*args):
    return -0.5 * tf.reduce_sum(1.0 + log_var - tf.square(mu) - tf.exp(log_var), 1)

def img_loss(y_true, y_pred):
    return tf.reduce_sum(tf.squared_difference(y_true, y_pred), 1) 

# Basic code to use history to plot graphs
def plot_history(history):
    pickle_file = os.path.join(checkpoint_dir, 'history.pickle')
    if os.path.exists(pickle_file):
        with open(pickle_file,'rb') as f: 
            old_history = pickle.load(f)
            for key in history.history:
                history.history[key] = old_history[key] + history.history[key]

            history.epoch = list(range(len(old_history['loss']))) + history.epoch

    with open(pickle_file, 'wb') as f:
        pickle.dump(history.history, f, pickle.HIGHEST_PROTOCOL)
        
    plt.grid()
    # start plotting from the 50th epoch, plot metrics from every 10th epoch after that
    plt.plot(history.epoch[50::10], history.history['loss'][50::10], color='r', label='Training Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Training Loss')
    plt.title('Epochs v. Training Loss Curve')
    plt.legend(loc='best')
    plt.savefig(os.path.join(checkpoint_dir, 'training_loss_curve.png'))
    plt.close()

    plt.grid()
    plt.plot(history.epoch[10:], history.history['val_loss'][10:], color='g', label='Validation Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Validation Loss')
    plt.title('Epochs v. Validation Loss Curve')
    plt.legend(loc='best')
    plt.savefig(os.path.join(checkpoint_dir, 'validation_loss_curve.png'))
    plt.close()

def create_model():
    global mu, log_var

    X_in = layers.Input(dtype='float32', shape=(input_dims,), name='X')
    Cond = layers.Input(dtype='float32', shape=(cond_dims,), name='Cond')
    d_in = layers.Input(dtype='float32', shape=(n_latent + cond_dims,), name='decoder_input')
    
    X_reshape = layers.Reshape((fps * gesture_length, n_features))(X_in)

    repeated_Cond = layers.RepeatVector(n_features)(Cond) # (n_features, cond_dims)
    permute_Cond = layers.Permute((2, 1))(repeated_Cond) # (cond_dims, n_features)

    XCond = layers.concatenate([X_reshape, permute_Cond], axis=1)

    sampled, mu, log_var = encoder(XCond, rate)
    sample_cond = layers.concatenate([sampled, Cond], axis=1)

    encoder_model = Model(inputs=[X_in, Cond], outputs=[mu, log_var])
    decoder_model = Model(inputs=d_in, outputs=decoder(d_in, rate))
    cvae = Model(inputs=[X_in, Cond], outputs=decoder_model(sample_cond))

    optimizer = tf.train.AdamOptimizer(0.0005)
    cvae.compile(optimizer=optimizer, loss=cvae_loss, metrics=[img_loss, latent_loss])

    # Save model architectures as JSON files that are used while freezing:
    with open(os.path.join(checkpoint_dir, 'cvae.json'), 'w') as f:
        f.write(cvae.to_json())

    with open(os.path.join(checkpoint_dir, 'encoder.json'), 'w') as f:
        f.write(encoder_model.to_json())

    with open(os.path.join(checkpoint_dir, 'decoder.json'), 'w') as f:
        f.write(decoder_model.to_json())

    return cvae

def fit_model(initial_epoch):
    vr_data = dataset_loading.read_timeseries_data_only_train(os.path.join(root_dir, vr_data_dir))

    X_train = vr_data.train.images.squeeze()
    y_train = vr_data.train.labels.squeeze()

    checkpoint_name = '{epoch:04d}-of-%04d-affordance-cvae.ckpt' % n_epochs
    cp_callback = callbacks.ModelCheckpoint(os.path.join(checkpoint_dir, checkpoint_name), save_weights_only=True, period=period, verbose=1)
    try:
        history = callbacks.History()
        model.fit([X_train, y_train], X_train, batch_size=batch_size, epochs=n_epochs, callbacks=[cp_callback, TrainingManager(), history], initial_epoch=initial_epoch, validation_split=0.1, verbose=0)
    finally:
        # save whatever we have in history no matter what
        model.stop_training = True
        plot_history(history)

# CONSTANTS:
root_dir = os.path.join('.', '..')
outputs_dir = 'outputs'
vr_data_dir = 'VR_data'

n_epochs = 2600
batch_size = 64
rate = 0.2
initial_epoch = 0
period = 100
max_to_keep = 2

input_dims = 27000
n_features = 30 # number of features in a single frame
fps = 90 # frames per second
gesture_length = 10 # length in seconds of each gesture

cond_dims = 25
mu = log_var = None

dec_in_channels = 1
n_latent = 2

reshaped_dim = [1, 64, dec_in_channels]
inputs_decoder = round(64 * dec_in_channels / 2)

checkpoint_dir = os.path.join(root_dir, outputs_dir) 
latest_checkpoint = tf.train.latest_checkpoint(checkpoint_dir)

model = create_model()

if latest_checkpoint:
  print('Found previously saved checkpoints...')
  if 'affordance-cvae-%d-%d-final.ckpt' % (input_dims, n_latent) in latest_checkpoint:
    print('Model already trained till max number of epochs = %04d! Program will exit now.' % n_epochs)
    sys.exit(0)
  initial_epoch = int(os.path.split(latest_checkpoint)[1].split('-of-')[0])
  print('Will use weights from checkpoint %s and resume training from epoch = %04d...' % (latest_checkpoint, initial_epoch))
  model.load_weights(latest_checkpoint)

fit_model(initial_epoch)
