import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import datetime

# from tensorflow.examples.tutorials.mnist import input_data
import dataset_loading

# mnist = input_data.read_data_sets('MNIST_data')
vr_data = dataset_loading.read_timeseries_data_sets('VR_data', dimensionality=9216)

tf.reset_default_graph()

batch_size = 64

X_in = tf.placeholder(dtype=tf.float32, shape=[None, 96, 96], name='X')
Y    = tf.placeholder(dtype=tf.float32, shape=[None, 96, 96], name='Y')
Y_flat = tf.reshape(Y, shape=[-1, 96 * 96])
keep_prob = tf.placeholder(dtype=tf.float32, shape=(), name='keep_prob')

dec_in_channels = 1
n_latent = 16

reshaped_dim = [-1, 16, 16, dec_in_channels]
inputs_decoder = round(256 * dec_in_channels / 2)


def lrelu(x, alpha=0.3):
    return tf.maximum(x, tf.multiply(x, alpha))

def encoder(X_in, keep_prob):
    activation = lrelu
    with tf.variable_scope("encoder", reuse=None):
        # print('In encoder')
        # print(tf.shape(X_in))
        X = tf.reshape(X_in, shape=[-1, 96, 96, 1])
        # print('After reshaping with shape=[-1, 96, 96, 1]')
        # print(tf.shape(X))
        x = tf.layers.conv2d(X, filters=32, kernel_size=4, strides=2, padding='same', activation=activation)
        # print('After 1st conv2d, filters=32, kernel_size=4, strides=2, padding=same, activation=lrelu')
        # print(tf.shape(x))
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d(x, filters=32, kernel_size=4, strides=2, padding='same', activation=activation)
        # print('After dropout and 2nd conv2d, filters=32, kernel_size=4, strides=2, padding=same, activation=lrelu')
        # print(tf.shape(x))
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d(x, filters=32, kernel_size=4, strides=1, padding='same', activation=activation)
        # print('After dropout and 3rd conv2d, filters=32, kernel_size=4, strides=1, padding=same, activation=lrelu')
        # print(tf.shape(x))
        x = tf.nn.dropout(x, keep_prob)
        x = tf.contrib.layers.flatten(x)
        # print('After dropout and flatten')
        # print(tf.shape(x))
        mn = tf.layers.dense(x, units=n_latent)
        sd       = 0.5 * tf.layers.dense(x, units=n_latent)            
        epsilon = tf.random_normal(tf.stack([tf.shape(x)[0], n_latent])) 
        z  = mn + tf.multiply(epsilon, tf.exp(sd))
        # print('mn')
        # print(tf.shape(mn))
        # print('sd')
        # print(tf.shape(sd))
        # print('epsilon')
        # print(tf.shape(epsilon))
        # print('z')
        # print(tf.shape(z))
        
        return z, mn, sd

def decoder(sampled_z, keep_prob):
    with tf.variable_scope("decoder", reuse=None):
        # print('In decoder')
        # print(tf.shape(sampled_z))
        x = tf.layers.dense(sampled_z, units=inputs_decoder, activation=lrelu)
        # print('After 1st dense layer, units=round(256 * dec_in_channels / 2)')
        # print(tf.shape(x))
        x = tf.layers.dense(x, units=inputs_decoder * 2, activation=lrelu)
        # print('After 2nd dense layer, units=round(256 * dec_in_channels / 2)')
        # print(tf.shape(x))
        x = tf.reshape(x, reshaped_dim)
        # print('After reshaping with shape=[-1, 16, 16, 1]')
        # print(tf.shape(x))
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=2, padding='same', activation=tf.nn.relu)
        # print('After 1st conv2d_transpose, filters=32, kernel_size=4, strides=2, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=1, padding='same', activation=tf.nn.relu)
        # print('After dropout and 2nd conv2d_transpose, filters=32, kernel_size=4, strides=1, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))
        x = tf.nn.dropout(x, keep_prob)
        x = tf.layers.conv2d_transpose(x, filters=32, kernel_size=4, strides=1, padding='same', activation=tf.nn.relu)
        # print('After dropout and 3rd conv2d_transpose, filters=32, kernel_size=4, strides=1, padding=same, activation=tf.nn.relu')
        # print(tf.shape(x))
        x = tf.contrib.layers.flatten(x)
        # print('After flatten')
        # print(tf.shape(x))
        x = tf.layers.dense(x, units=96*96, activation=tf.nn.sigmoid)
        # print('After dense layer, units=96*96, activation=tf.nn.sigmoid')
        # print(tf.shape(x))
        img = tf.reshape(x, shape=[-1, 96, 96])
        # print('After reshaping with shape=[-1, 96, 96]')
        # print(tf.shape(img))
        return img

sampled, mn, sd = encoder(X_in, keep_prob)
dec = decoder(sampled, keep_prob)

unreshaped = tf.reshape(dec, [-1, 96*96])
# print('Decoder output after reshaping with shape=[-1, 96, 96]')
# print(tf.shape(unreshaped))
img_loss = tf.reduce_sum(tf.squared_difference(unreshaped, Y_flat), 1)
# print('Image loss')
# print(tf.shape(img_loss))
latent_loss = -0.5 * tf.reduce_sum(1.0 + 2.0 * sd - tf.square(mn) - tf.exp(2.0 * sd), 1)
# print('Latent loss')
# print(tf.shape(latent_loss))
loss = tf.reduce_mean(img_loss + latent_loss)
# print('Total loss')
# print(tf.shape(loss))
optimizer = tf.train.AdamOptimizer(0.0005).minimize(loss)
sess = tf.Session()
sess.run(tf.global_variables_initializer())

print(str(datetime.datetime.now()))
for i in range(10000):
    batch = [np.reshape(b, [96, 96]) for b in vr_data.train.next_timeseries_batch(batch_size=batch_size)[0]]
    # batchprint = np.asarray(batch)
    # print('Batch after reshaping with shape=b, [96, 96]')
    # print(tf.shape(batchprint))
    # print('Batch')
    # print(batchprint)
    sess.run(optimizer, feed_dict = {X_in: batch, Y: batch, keep_prob: 0.8})
        
    if not i % 10:
        ls, d, i_ls, d_ls, mu, sigm = sess.run([loss, dec, img_loss, latent_loss, mn, sd], feed_dict = {X_in: batch, Y: batch, keep_prob: 1.0})
        plt.imshow(np.reshape(batch[0], [96, 96]), cmap='gray')
        plt.savefig('outputs/'+ str(i) +'-original.png', bbox_inches='tight')
        # print('Original image after reshape with [batch[0], [96, 96]]')
        # print(np.reshape(batch[0], [96, 96]).shape)
        # print('Original image after reshape with [batch[0], [96, 96]]')
        # print(np.reshape(batch[0], [96, 96]))
        with open('outputs/' + str(i) + '-original.csv', 'w') as csvfile:
            np.savetxt(csvfile, np.reshape(batch[0], [96, 96]), delimiter=',')
        plt.imshow(d[0], cmap='gray')
        plt.savefig('outputs/'+ str(i) +'-vae.png', bbox_inches='tight')
        # print('Generated image')
        # print(tf.shape(d[0]))
        # print('Generated image')
        # print(d[0])
        with open('outputs/' + str(i) + '-vae.csv', 'w') as csvfile:
            np.savetxt(csvfile, d[0], delimiter=',')
        # print('Image loss')
        # print(i_ls)
        print(i, ls, np.mean(i_ls), np.mean(d_ls))

    if not i % 100:
        randoms = [np.random.normal(0, 1, n_latent) for _ in range(10)]
        # randomprint = np.asarray(randoms)
        # print('Random samples')
        # print(tf.shape(randomprint))
        # print('Random samples')
        # print(randomprint)
        imgs = sess.run(dec, feed_dict = {sampled: randoms, keep_prob: 1.0})
        imgs = [np.reshape(imgs[i], [96, 96]) for i in range(len(imgs))]
        # imgsprint = np.asarray(imgs)
        # print('Images')
        # print(tf.shape(imgsprint))
        j = 0
        # print(len(imgs))
        for img in imgs:
            imgprint = np.asarray(img)
            # print('Image')
            # print(tf.shape(imgprint))
            with open('outputs/' + str(i) + '-vae-generated-'+ str(j) +'.csv', 'w') as csvfile:
                np.savetxt(csvfile, imgprint, delimiter=',')
            # plt.figure(figsize=(1,1))
            # plt.axis('off')
            plt.imshow(img, cmap='gray')
            plt.savefig('outputs/'+ str(i) +'-vae-generated-'+ str(j) +'.png', bbox_inches='tight')
            j += 1

print(str(datetime.datetime.now()))

randoms = [np.random.normal(0, 1, n_latent) for _ in range(100)]
imgs = sess.run(dec, feed_dict = {sampled: randoms, keep_prob: 1.0})
imgs = [np.reshape(imgs[i], [96, 96]) for i in range(len(imgs))]

j = 0
for img in imgs:
    # plt.figure(figsize=(1,1))
    # plt.axis('off')
    imgprint = np.asarray(img)
    with open('outputs/final-vae-generated-' + str(j) + '.csv', 'w') as csvfile:
        np.savetxt(csvfile, imgprint, delimiter=',')
    plt.imshow(img, cmap='gray')
    plt.savefig('outputs/final-vae-generated-' + str(j) + '.png', bbox_inches='tight')
    j += 1

print(str(datetime.datetime.now()))
